import { HttpRequest, urlApi } from '../enum/enum';
import { api } from './apiHelper';
import { IQuery } from '../interfaces/interfaces';

export const getData = async (method: HttpRequest, url: urlApi, queryString?: IQuery, id = '') => {
    const data = await api(method, {
        query: queryString?.query,
        page: queryString?.page,
        api_key: '8979ac0487c3ded02eabdd491e731df8',
    }, url + id);
    return data;
}
