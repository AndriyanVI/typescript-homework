import { HttpRequest, urlApi, movieContainer } from "../enum/enum";
import { IMovie } from "../interfaces/interfaces";
import { getData } from "./request";

export const createElementMovie = async (container: movieContainer, movie: IMovie) => {
    const favouriteNode = document.getElementById(movieContainer.FAVOURITE);
    const element = document.createElement('div');
    const elementChild = document.createElement('div');
    elementChild.classList.add('card', 'shadow-sm');

    const imgElement = document.createElement('img');
    if (movie.poster_path === null) {
        imgElement.src = 'https://worldsrc.uproxy.co/assets/images/no-poster.png'
    } else {
        imgElement.src = `https://image.tmdb.org/t/p/original${movie.poster_path}`;
    }

    const svgElement = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
    svgElement.setAttribute('stroke', 'red');

    if (container === movieContainer.FILM) {
        element.classList.add('col-lg-3', 'col-md-4', 'col-12', 'p-2');
        svgElement.setAttribute('id', movie.id.toString());
        if (localStorage.getItem(svgElement.id)) {
            svgElement.setAttribute('fill', '#ff0000');
        } else {
            svgElement.setAttribute('fill', '#ff000078');
        }
        svgElement.addEventListener('click', async () => {
            if (localStorage.getItem(svgElement.id)) {
                svgElement.setAttribute('fill', '#ff000078');
                localStorage.removeItem(svgElement.id);
                const favouriteSvg = document.getElementById('f' + svgElement.id);
                const elementDelete = favouriteSvg?.parentElement?.parentElement;
                favouriteNode?.removeChild(<HTMLElement>elementDelete);

            } else {
                svgElement.setAttribute('fill', '#ff0000');
                localStorage.setItem(svgElement.id, svgElement.id);

                const movie = await getData(HttpRequest.GET, urlApi.MOVIE, undefined, svgElement.id);
                createElementMovie(movieContainer.FAVOURITE, movie);
            }

            console.log(Object.entries(localStorage));

        });

    } else if (container === movieContainer.FAVOURITE) {
        element.classList.add('col-12', 'p-2');
        svgElement.setAttribute('id', 'f' + movie.id);
        svgElement.setAttribute('fill', '#ff0000');
        svgElement.addEventListener('click', async () => {
            const id = svgElement.id.substring(1);
            const elemOnPage = document.getElementById(id);
            if (elemOnPage) {
                elemOnPage.setAttribute('fill', '#ff000078');
            }
            localStorage.removeItem(id);
            favouriteNode?.removeChild(<HTMLElement>svgElement.parentElement?.parentElement);
        });
    }


    svgElement.setAttribute('width', '50');
    svgElement.setAttribute('height', '50');
    svgElement.classList.add('bi', 'bi-heart-fill', 'position-absolute', 'p-2');
    svgElement.setAttribute('viewBox', '0 -2 18 22');


    const pathElem = document.createElementNS('http://www.w3.org/2000/svg', 'path');
    pathElem.setAttribute('fill-rule', 'evenodd');
    pathElem.setAttribute('d', 'M8 1.314C12.438-3.248 23.534 4.735 8 15-7.534 4.736 3.562-3.248 8 1.314z');

    const textDiv = document.createElement('div');
    textDiv.classList.add('card-body');
    const paragraph = document.createElement('p');

    paragraph.classList.add('card-text', 'truncate');
    paragraph.innerHTML = movie.overview || 'No overview';

    const flexDiv = document.createElement('div');
    flexDiv.classList.add('d-flex', 'justify-content-between', 'align-items-center');

    const smallElem = document.createElement('small');
    smallElem.classList.add('text-muted');
    smallElem.innerHTML = movie.release_date || "Unknown release date";


    flexDiv.appendChild(smallElem);
    textDiv.appendChild(paragraph);
    textDiv.appendChild(flexDiv);
    svgElement.appendChild(pathElem);
    elementChild.append(imgElement);
    elementChild.append(svgElement);
    elementChild.append(textDiv);
    element.appendChild(elementChild);
    console.log(element);

    const containerNode = document.getElementById(container);
    containerNode?.appendChild(element);
}

export const clearMainContent = (): void => {
    const myNode = document.getElementById("film-container");
    while (myNode?.firstChild) {
        myNode.removeChild(<Node>myNode.lastChild);
    }
}