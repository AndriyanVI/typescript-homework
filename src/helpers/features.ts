import { HttpRequest, urlApi, Button, movieContainer } from "../enum/enum";
import { IMovieList, IMovie } from "../interfaces/interfaces";
import { createElementMovie } from "./createMovieElement";
import { movieListMapper } from "./mapper";
import { getData } from "./request";

export const createLoadButton = (page: number, total_pages: number, query: string, apiUrl: urlApi) => {
    const previousButton = document.getElementById(Button.LOAD_MORE);
    const newButton = <HTMLButtonElement>(previousButton?.cloneNode(true));
    previousButton?.parentElement?.appendChild(newButton);
    previousButton?.parentElement?.removeChild(previousButton);

    if ((total_pages === 0) || (total_pages === page)) {
        newButton.style.display = 'none';
        return;
    } else {
        newButton.style.display = 'block    ';
    }


    newButton.addEventListener('click', async () => {

        page += 1;
        let movies: IMovieList;
        if (apiUrl === urlApi.SEARCH) {
            movies = movieListMapper(await getData(HttpRequest.GET, apiUrl, { query: query, page: page }));
        } else {
            movies = movieListMapper(await getData(HttpRequest.GET, apiUrl, { page: page }));
        }
        movies.results.forEach((movie) => {
            createElementMovie(movieContainer.FILM, movie);
        });
        if (page === total_pages) newButton.style.display = 'none';



    })


}

export const setRandomMovie = (movies: IMovie[]) => {
    movies = movies.filter((movie) => movie.backdrop_path && movie.overview);
    if (movies.length !== 0) {
        const randomMovieDiv = <HTMLDivElement>document.getElementById('random-movie');
        const randomNumber = getRandomInt(movies.length);

        randomMovieDiv.style.backgroundImage = `url(https://image.tmdb.org/t/p/original${movies[randomNumber].backdrop_path})`;
        randomMovieDiv.style.backgroundSize = 'contain';

        const randomMovieHeader = <HTMLHeadingElement>document.getElementById('random-movie-name');
        randomMovieHeader.innerHTML = <string>movies[randomNumber].title;

        const randomDesctiption = <HTMLHeadingElement>document.getElementById('random-movie-description');
        randomDesctiption.innerHTML = <string>movies[randomNumber].overview;

    }

}

const getRandomInt = (max: number) => {
    return Math.floor(Math.random() * max);
}