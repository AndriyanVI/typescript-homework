import { stringifyUrl } from "query-string";
import { HttpRequest } from "../enum/enum";



export const api = async (method: HttpRequest, query: { api_key: string, page?: number, query?: string }, apiUrl: string) => {
    const response = await fetch(stringifyUrl({ url: apiUrl, query }, { skipNull: true }), { method });
    const data = await response.json();
    return data;
}