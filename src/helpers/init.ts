import { HttpRequest, urlApi, Button, movieContainer } from "../enum/enum";
import { IMovieList } from "../interfaces/interfaces";
import { createElementMovie, clearMainContent } from "./createMovieElement";
import { createLoadButton, setRandomMovie } from "./features";
import { movieListMapper } from "./mapper";
import { getData } from "./request";

export const initRequest = async (button: Button, apiMethod: HttpRequest, apiUrl: urlApi, page?: number) => {
    const pressedButton = document.getElementById(button);
    pressedButton?.addEventListener('click', async () => {
        let movies: IMovieList;
        let query = '';
        if (button === Button.SEARCH) {
            query = (<HTMLInputElement>document.getElementById('search')).value;
            movies = movieListMapper(await getData(apiMethod, apiUrl, { query: query, page: page }));
        } else {
            movies = movieListMapper(await getData(apiMethod, apiUrl));
        }
        setRandomMovie(movies.results);
        clearMainContent();
        movies.results.forEach((movie) => {
            createElementMovie(movieContainer.FILM, movie);
        });
        createLoadButton(movies.page, movies.total_pages, query, apiUrl);
    })
}

export const initPage = async () => {
    document.getElementById(Button.POPULAR)?.click();
};