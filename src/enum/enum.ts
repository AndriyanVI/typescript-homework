export enum HttpRequest {
    GET = 'GET',
    PUT = 'PUT',
    POST = 'POST',
    DELETE = 'DELETE',
}

export enum urlApi {
    MOVIE = 'https://api.themoviedb.org/3/movie/',
    SEARCH = 'https://api.themoviedb.org/3/search/movie',
    POPULAR = 'https://api.themoviedb.org/3/movie/popular',
    TOP_RATED = 'https://api.themoviedb.org/3/movie/top_rated',
    UPCOMING = 'https://api.themoviedb.org/3/movie/upcoming',

}

export enum Button {
    SEARCH = 'submit',
    POPULAR = 'popular',
    TOP_RATED = 'top_rated',
    UPCOMING = 'upcoming',
    LOAD_MORE = 'load-more',
}

export enum movieContainer {
    FAVOURITE = 'favorite-movies',
    FILM = 'film-container'
}