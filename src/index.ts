import { initFavorite } from "./components/favourite";
import { initSearch } from "./components/search";
import { initPopular } from "./components/popular";
import { initTopRated } from "./components/topRated";
import { initUpcoming } from "./components/upcoming";
import { initPage } from "./helpers/init";

export async function render(): Promise<void> {
    await initSearch();
    await initPopular();
    await initFavorite();
    await initSearch();
    await initPopular();
    await initTopRated();
    await initUpcoming()
    await initPage();
}
