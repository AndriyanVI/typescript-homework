import { HttpRequest, urlApi, Button } from "../enum/enum";
import { initRequest } from "../helpers/init";

export const initTopRated = async () => {
    await initRequest(Button.TOP_RATED, HttpRequest.GET, urlApi.TOP_RATED);
};