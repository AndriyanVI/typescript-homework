import { movieContainer, HttpRequest, urlApi } from "../enum/enum";
import { createElementMovie } from "../helpers/createMovieElement";
import { movieMapper } from "../helpers/mapper";
import { getData } from "../helpers/request";
import { IMovie } from "../interfaces/interfaces";

export const initFavorite = async () => {
    const favouriteNode = <HTMLElement>document.getElementById(movieContainer.FAVOURITE);
    while (favouriteNode?.firstChild) {
        favouriteNode.removeChild(<Node>favouriteNode.lastChild);
    }

    Object.keys(localStorage).forEach(async (key) => {
        if (key !== 'undefined') {
            const movie: IMovie = movieMapper(await getData(HttpRequest.GET, urlApi.MOVIE, undefined, key));
            createElementMovie(movieContainer.FAVOURITE, movie);
        }
    });
};