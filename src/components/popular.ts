import { HttpRequest, urlApi, Button } from "../enum/enum";
import { initRequest } from "../helpers/init";

export const initPopular = async () => {
    await initRequest(Button.POPULAR, HttpRequest.GET, urlApi.POPULAR);
}