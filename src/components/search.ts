import { Button, HttpRequest, urlApi } from "../enum/enum";
import { initRequest } from "../helpers/init";

export const initSearch = async () => {
    await initRequest(Button.SEARCH, HttpRequest.GET, urlApi.SEARCH);
};