import { HttpRequest, urlApi, Button } from "../enum/enum";
import { initRequest } from "../helpers/init";

export const initUpcoming = async () => {
    await initRequest(Button.UPCOMING, HttpRequest.GET, urlApi.UPCOMING);
};